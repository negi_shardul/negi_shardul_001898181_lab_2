/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Shardul 
 */
public class VitalSigns {
  
    
    
    // Attributes for the vital signs 
    private double temprature;

    public double getTemprature() {
        return temprature;
    }

    public void setTemprature(double temprature) {
        this.temprature = temprature;
    }

    public double getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(double bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public int getPulse() {
        return pulse;
    }

    public void setPulse(int pulse) {
        this.pulse = pulse;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
private double bloodPressure;
private int pulse; 
private String data;

@Override
public String toString()
{
    return this.data;
}

}
