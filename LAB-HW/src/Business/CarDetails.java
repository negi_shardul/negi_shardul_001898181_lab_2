/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Shardul Negi
 */
public class CarDetails {
  
    
    //defining all the atributes for the class
    private String sDriversName;

    public String getsDriversName() {
        return sDriversName;
    }

    public void setsDriversName(String sDriversName) {
        this.sDriversName = sDriversName;
    }
    private String sCarName;

    public String getsCarName() {
        return sCarName;
    }

    public void setsCarName(String sCarName) {
        this.sCarName = sCarName;
    }
    private String sManufacturer;
    private int iYear;
    private int iNoOfSeats;
    private String sSerialNo;
    private String sModelNo;
    private boolean bOccupied[];// a cab will have some seats so an array to compare that 
    private String sCity;
    private String sMaintainaceCertiDate;

    public String getsManufacturer() {
        return sManufacturer;
    }

    public void setsManufacturer(String sManufacturer) {
        this.sManufacturer = sManufacturer;
    }

    public int getiYear() {
        return iYear;
    }

    public void setiYear(int iYear) {
        this.iYear = iYear;
    }

    public int getiNoOfSeats() {
        return iNoOfSeats;
    }

    public void setiNoOfSeats(int iNoOfSeats) {
        this.iNoOfSeats = iNoOfSeats;
    }

    public String getsSerialNo() {
        return sSerialNo;
    }

    public void setsSerialNo(String sSerialNo) {
        this.sSerialNo = sSerialNo;
    }

    public String getsModelNo() {
        return sModelNo;
    }

    public void setsModelNo(String sModelNo) {
        this.sModelNo = sModelNo;
    }

    public boolean[] getbOccupied() {
        return bOccupied;
    }

    public void setbOccupied(boolean[] bOccupied) {
        this.bOccupied = bOccupied;
    }

    public String getsCity() {
        return sCity;
    }

    public void setsCity(String sCity) {
        this.sCity = sCity;
    }

    public String getsMaintainaceCertiDate() {
        return sMaintainaceCertiDate;
    }

    public void setsMaintainaceCertiDate(String sMaintainaceCertiDate) {
        this.sMaintainaceCertiDate = sMaintainaceCertiDate;
    }
    
    
    
    @Override
    public String toString(){
       return this.sDriversName; 
    }
    
}
