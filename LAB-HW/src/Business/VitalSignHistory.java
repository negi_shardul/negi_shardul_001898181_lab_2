/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Shardul 
 */
public class VitalSignHistory {


    
    
    //coonstructer to initialize the variable for arraylist 
public VitalSignHistory()
{
      
    vitalSignHistory= new ArrayList<VitalSigns>();
}
    
    private ArrayList<VitalSigns> vitalSignHistory;

    public ArrayList<VitalSigns> getVitalSignHistory() {
        return vitalSignHistory;
    }

    public void setVitalSignHistory(ArrayList<VitalSigns> vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }

    
    // to add the vital signs to teh array list 
    
    
    public VitalSigns addVitals()
    {
        //creating an object for the vital sign and not adding the new values to the Vital sign
        VitalSigns vital = new VitalSigns();
        vitalSignHistory.add(vital);    
        return vital;
    }
    
    public void removeVitals(VitalSigns v){
        
       vitalSignHistory.remove(v);
    }    
    
    
    
}
