/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.VitalSignHistory;
import Business.VitalSigns;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Dell
 */
public class ViewVitalsPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewVitals
     */
    
    private VitalSignHistory vitalHistory;
    public ViewVitalsPanel(VitalSignHistory vitalHistory) {
        initComponents();
        this.vitalHistory= vitalHistory;
        populateTable();
    }

    //Code to populate table 
    
    public void populateTable(){
        //The table will get refreshed like this 
         DefaultTableModel dtm= (DefaultTableModel) tblVitalSign.getModel();
         dtm.setRowCount(0);//  will insure 0that noo rows are there in the table 
         
         //Now display all the vital details 
         for(VitalSigns v : vitalHistory.getVitalSignHistory()){
           Object row[]= new Object[2];
            row[0]=v ;
            row[1]=v.getBloodPressure();
            dtm.addRow(row);
            
            
         }         
         
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewHeaderLbl = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblVitalSign = new javax.swing.JTable();
        viewDetailsBtn = new javax.swing.JButton();
        deleteBtn = new javax.swing.JButton();
        tempratureLbl = new javax.swing.JLabel();
        bloodPressureLbl = new javax.swing.JLabel();
        pulseLbl = new javax.swing.JLabel();
        dateLbl = new javax.swing.JLabel();
        tempratureTxtBx = new javax.swing.JTextField();
        bloodPressureTxtBx = new javax.swing.JTextField();
        pulseTxtBx = new javax.swing.JTextField();
        dateTxtBx = new javax.swing.JTextField();

        viewHeaderLbl.setFont(new java.awt.Font("Times New Roman", 0, 24)); // NOI18N
        viewHeaderLbl.setForeground(new java.awt.Color(0, 0, 255));
        viewHeaderLbl.setText("View Vital Signs");

        tblVitalSign.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Blood Pressure"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblVitalSign);

        viewDetailsBtn.setText("View Details");
        viewDetailsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewDetailsBtnActionPerformed(evt);
            }
        });

        deleteBtn.setText("Delete");
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });

        tempratureLbl.setText("Teprature:");

        bloodPressureLbl.setText("Blood Pressure: ");

        pulseLbl.setText("Pulse:");

        dateLbl.setText("Date:");

        tempratureTxtBx.setEnabled(false);
        tempratureTxtBx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tempratureTxtBxActionPerformed(evt);
            }
        });

        bloodPressureTxtBx.setEnabled(false);

        pulseTxtBx.setEnabled(false);
        pulseTxtBx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulseTxtBxActionPerformed(evt);
            }
        });

        dateTxtBx.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(211, 211, 211)
                        .addComponent(viewHeaderLbl))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 438, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(178, 178, 178)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(viewDetailsBtn)
                            .addComponent(tempratureLbl)
                            .addComponent(bloodPressureLbl)
                            .addComponent(pulseLbl)
                            .addComponent(dateLbl))
                        .addGap(78, 78, 78)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(deleteBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tempratureTxtBx)
                            .addComponent(bloodPressureTxtBx)
                            .addComponent(pulseTxtBx)
                            .addComponent(dateTxtBx))))
                .addContainerGap(364, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(viewHeaderLbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(viewDetailsBtn)
                    .addComponent(deleteBtn))
                .addGap(69, 69, 69)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tempratureLbl)
                    .addComponent(tempratureTxtBx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bloodPressureLbl)
                    .addComponent(bloodPressureTxtBx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pulseLbl)
                    .addComponent(pulseTxtBx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dateLbl)
                    .addComponent(dateTxtBx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(80, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtnActionPerformed
        // TODO add your handling code here:
         int selectRow= tblVitalSign.getSelectedRow();
        
        if(selectRow >=0){
            VitalSigns vs= (VitalSigns)tblVitalSign.getValueAt(selectRow, 0);
            vitalHistory.removeVitals(vs);
            JOptionPane.showMessageDialog(null,"The selected record has been deleted");
            populateTable();
        }
        else{
            JOptionPane.showMessageDialog(null,"Please select a Row to View");
        }
        
        
        
        
    }//GEN-LAST:event_deleteBtnActionPerformed

    private void pulseTxtBxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulseTxtBxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pulseTxtBxActionPerformed

    private void tempratureTxtBxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tempratureTxtBxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tempratureTxtBxActionPerformed

    private void viewDetailsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewDetailsBtnActionPerformed
        // TODO add your handling code here:
       // populateTable();
       try{
        int selectRow= tblVitalSign.getSelectedRow();
        
        //tblVitalSign.getValueAt(ERROR, WIDTH)
        System.out.print("Row No. : "+selectRow + " Row Count:"+tblVitalSign.getRowCount() +"Row selected: "+ tblVitalSign.isRowSelected(selectRow)+ "\n"+
                tblVitalSign.toString());
       
        
        if(selectRow >=0){
          VitalSigns vs = (VitalSigns)tblVitalSign.getValueAt(selectRow, 0);
            bloodPressureTxtBx.setText(String.valueOf(vs.getBloodPressure()));
            tempratureTxtBx.setText(String.valueOf(vs.getTemprature()));
            pulseTxtBx.setText(String.valueOf(vs.getPulse()));
            dateTxtBx.setText(vs.getData());
        }
        else{
            JOptionPane.showMessageDialog(null,"Please select a Row to View");
        }
       }
       catch(Exception e){
            JOptionPane.showMessageDialog(null,"Please select a Row again with ewith except"
                    + "tion"+e);
            
       }
        
    }//GEN-LAST:event_viewDetailsBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bloodPressureLbl;
    private javax.swing.JTextField bloodPressureTxtBx;
    private javax.swing.JLabel dateLbl;
    private javax.swing.JTextField dateTxtBx;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel pulseLbl;
    private javax.swing.JTextField pulseTxtBx;
    private javax.swing.JTable tblVitalSign;
    private javax.swing.JLabel tempratureLbl;
    private javax.swing.JTextField tempratureTxtBx;
    private javax.swing.JButton viewDetailsBtn;
    private javax.swing.JLabel viewHeaderLbl;
    // End of variables declaration//GEN-END:variables
}
