/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Product;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Dell
 */
public class CreateJPanel extends javax.swing.JPanel {

    /**
     * Creates new form CreateJPanel
     */
    
    
    
    //initialization of the Product Class to fetch values andn save the same 
    private Product product;
    
    // Parameterized constructer for passing the value entered to the program
    public CreateJPanel(Product product) {
        initComponents();
        this.product= product;
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelHeader = new javax.swing.JLabel();
        labelName = new javax.swing.JLabel();
        labelGeographicData = new javax.swing.JLabel();
        labelDOB = new javax.swing.JLabel();
        labelTelephoneNo = new javax.swing.JLabel();
        labelEmail = new javax.swing.JLabel();
        labelSSN = new javax.swing.JLabel();
        labelMedical = new javax.swing.JLabel();
        labelBankAccn = new javax.swing.JLabel();
        labelLinkedn = new javax.swing.JLabel();
        labelIPAdd = new javax.swing.JLabel();
        txtFieldName = new javax.swing.JTextField();
        txtFieldGeographic = new javax.swing.JTextField();
        txtFieldDOB = new javax.swing.JTextField();
        txtFieldTelephoneNo = new javax.swing.JTextField();
        txtFieldEmail = new javax.swing.JTextField();
        txtFieldSSN = new javax.swing.JTextField();
        txtFieldMedical = new javax.swing.JTextField();
        txtFieldBankAccn = new javax.swing.JTextField();
        txtFieldLinkedn = new javax.swing.JTextField();
        txtFieldIPAddress = new javax.swing.JTextField();
        btnCreateProfile = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        labelHeader.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        labelHeader.setText("Create The Personal Profile");

        labelName.setText("Name:");

        labelGeographicData.setText("Geographic Data: ");

        labelDOB.setText("D.O.B: ");

        labelTelephoneNo.setText("Telephone No.:");

        labelEmail.setText("Email Address:");

        labelSSN.setText("SSN :");

        labelMedical.setText("Medical Record:");

        labelBankAccn.setText("Bank Account No:");

        labelLinkedn.setText("LinkedIn: ");

        labelIPAdd.setText("IP address: ");

        txtFieldName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFieldNameActionPerformed(evt);
            }
        });

        txtFieldGeographic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFieldGeographicActionPerformed(evt);
            }
        });

        btnCreateProfile.setText("Create Profile");
        btnCreateProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateProfileActionPerformed(evt);
            }
        });

        jButton1.setText("Upload Profile");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(labelName)
                        .addGap(75, 75, 75)
                        .addComponent(txtFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(labelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelGeographicData)
                            .addComponent(labelDOB)
                            .addComponent(labelTelephoneNo)
                            .addComponent(labelEmail)
                            .addComponent(labelSSN)
                            .addComponent(labelMedical))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtFieldDOB, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtFieldMedical, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                                    .addComponent(txtFieldSSN, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtFieldEmail, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtFieldTelephoneNo, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtFieldGeographic))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelLinkedn)
                                    .addComponent(labelIPAdd)
                                    .addComponent(labelBankAccn))
                                .addGap(5, 5, 5)))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtFieldLinkedn, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtFieldIPAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 26, Short.MAX_VALUE))
                            .addComponent(txtFieldBankAccn, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addGap(29, 29, 29)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCreateProfile)
                .addGap(101, 101, 101))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(labelHeader)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelName)
                    .addComponent(txtFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelGeographicData)
                    .addComponent(txtFieldGeographic, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelDOB)
                    .addComponent(txtFieldDOB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelTelephoneNo)
                    .addComponent(txtFieldTelephoneNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEmail)
                    .addComponent(txtFieldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelIPAdd)
                    .addComponent(txtFieldIPAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelSSN)
                    .addComponent(txtFieldSSN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelBankAccn)
                    .addComponent(txtFieldBankAccn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelMedical)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtFieldMedical, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelLinkedn)
                        .addComponent(txtFieldLinkedn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addComponent(btnCreateProfile)
                .addGap(37, 37, 37))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtFieldNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFieldNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFieldNameActionPerformed

    private void txtFieldGeographicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFieldGeographicActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFieldGeographicActionPerformed

    
    
    //Will be used when we will click on the Create button 
    private void btnCreateProfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateProfileActionPerformed
        // TODO add your handling code here:
        //Assigning values to each fields
        product.setName(txtFieldName.getText()); 
        product.setGeographicData(txtFieldGeographic.getText());
        product.setTelephoneNumber(txtFieldTelephoneNo.getText());
        product.setUniqueCharacteristics(txtFieldDOB.getText());
        product.setFaxNumber(txtFieldMedical.getText());
        product.setEmailAddress(txtFieldEmail.getText());// Faux
        product.setBankAccountNumber(txtFieldBankAccn.getText());
        product.setLinkdn(txtFieldLinkedn.getText());
        product.setsIPAddress(txtFieldIPAddress.getText());
        product.setFaxNumber(txtFieldMedical.getText());

        
        //Pop up to check the values
        
        JOptionPane.showMessageDialog(null, "Profile was created Successfully!! \n Press OK"
                                             + "to move to the Main Frame");        
    }//GEN-LAST:event_btnCreateProfileActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        JFileChooser choose= new JFileChooser();
        choose.showOpenDialog(null);
        File file= choose.getSelectedFile();
        String f= file.getAbsolutePath();
        jTextField1.setText(f);
        
        ImageIcon icon = new ImageIcon(f);
        jButton1.setIcon(icon);
       
        
        
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreateProfile;
    private javax.swing.JButton jButton1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel labelBankAccn;
    private javax.swing.JLabel labelDOB;
    private javax.swing.JLabel labelEmail;
    private javax.swing.JLabel labelGeographicData;
    private javax.swing.JLabel labelHeader;
    private javax.swing.JLabel labelIPAdd;
    private javax.swing.JLabel labelLinkedn;
    private javax.swing.JLabel labelMedical;
    private javax.swing.JLabel labelName;
    private javax.swing.JLabel labelSSN;
    private javax.swing.JLabel labelTelephoneNo;
    private javax.swing.JTextField txtFieldBankAccn;
    private javax.swing.JTextField txtFieldDOB;
    private javax.swing.JTextField txtFieldEmail;
    private javax.swing.JTextField txtFieldGeographic;
    private javax.swing.JTextField txtFieldIPAddress;
    private javax.swing.JTextField txtFieldLinkedn;
    private javax.swing.JTextField txtFieldMedical;
    private javax.swing.JTextField txtFieldName;
    private javax.swing.JTextField txtFieldSSN;
    private javax.swing.JTextField txtFieldTelephoneNo;
    // End of variables declaration//GEN-END:variables
}
