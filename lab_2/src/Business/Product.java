/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Dell
 */
public class Product {
  
 private String name;//
 private String geographicData;
 private String telephoneNumber;
 private String emailAddress;
 private String linkdn;
 private String socialSecurity;
 private String sIPAddress;
 private String uniqueCharacteristics;//DOB
 private String bankAccountNumber;
 private String faxNumber;//Medical Record

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeographicData() {
        return geographicData;
    }

    public void setGeographicData(String geographicData) {
        this.geographicData = geographicData;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getLinkdn() {
        return linkdn;
    }

    public void setLinkdn(String linkdn) {
        this.linkdn = linkdn;
    }

    public String getSocialSecurity() {
        return socialSecurity;
    }

    public void setSocialSecurity(String socialSecurity) {
        this.socialSecurity = socialSecurity;
    }

    public String getsIPAddress() {
        return sIPAddress;
    }

    public void setsIPAddress(String sIPAddress) {
        this.sIPAddress = sIPAddress;
    }

    public String getUniqueCharacteristics() {
        return uniqueCharacteristics;
    }

    public void setUniqueCharacteristics(String uniqueCharacteristics) {
        this.uniqueCharacteristics = uniqueCharacteristics;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }
    
}
